package uz.magichands.universaldictionary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.magichands.universaldictionary.domain.Word;

@Repository
public interface WordRepository extends JpaRepository<Word, Long> {
}
