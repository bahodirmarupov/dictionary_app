package uz.magichands.universaldictionary.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uz.magichands.universaldictionary.payload.WordReq;
import uz.magichands.universaldictionary.payload.yandex.DictionaryWordApi;

@Service
public class WordService {
    @Value("${dictionary.api.key}")
    private String apiKey;
    @Autowired
    private RestTemplate restTemplate;
//    @Autowired WordRes

    public DictionaryWordApi checkWord(WordReq wordReq) {
        DictionaryWordApi wordApi = restTemplate.getForEntity(String.format(
                "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=%s&lang=%s&text=%s", apiKey, wordReq.isEnglish() ? "en-uz" : "uz-en", wordReq.getWord()),
                DictionaryWordApi.class
        ).getBody();
        return wordApi;


    }
}
