package uz.magichands.universaldictionary;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;
import uz.magichands.universaldictionary.payload.yandex.DictionaryWordApi;

@SpringBootTest
class UniversalDictionaryApplicationTests {

    @Autowired
    RestTemplate restTemplate;
    @Test
    void contextLoads() {
        DictionaryWordApi wordApi = restTemplate.getForEntity(String.format(
                "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=%s&lang=%s&text=%s", "dict.1.1.20201101T173105Z.de7d4c300955ab0c.7c4963c3add39c18753cc928cc9758e3b2e398a6", "en-ru","time" ),
                DictionaryWordApi.class).getBody();
        assert wordApi != null;
        System.out.println(wordApi.getDef().get(0).getText());
    }

}
